package fr.esigelec.jee.dao;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.esigelec.jee.HashingUtil;

public class DBDAO {
    private Connection conn = null;

    private Map<String, String> countries;
    private Map<String, String> cities;
    private List<String> countryCodes;

    public DBDAO() {
        // Initialize the maps and list
        countries = new HashMap<>();
        cities = new HashMap<>();
        countryCodes = new ArrayList<>();

        // Hard-coded values for the example
        countries.put("PT", "Portugal");
        countries.put("FR", "France");
        countries.put("ES", "Espagne");

        cities.put("PT", "Lisbon");
        cities.put("FR", "Paris");
        cities.put("ES", "Madrid");

        countryCodes.add("PT");
        countryCodes.add("FR");
        countryCodes.add("ES");
    }

    public List<String> getCountryCodes() {
        return new ArrayList<>(countryCodes); // return a copy to protect original list
    }

    public String getCountryName(String countryCode) {
        return countries.get(countryCode);
    }

    public String getCityName(String countryCode) {
        return cities.get(countryCode);
    }
    static {
        try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            // Handle the error here. Perhaps log it and throw a custom exception
            e.printStackTrace();
        }
    }
    private void dbConnect() throws SQLException {
        
        String DB_URL = "jdbc:mysql://localhost:3306/countries?useSSL=false&serverTimezone=UTC";
        String USER = "ccodes";
        String PASS = "javaee2021";
        
        // Attempt to establish a connection to the database
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
    }

    private void dbClose() throws SQLException {
        // Close the connection if it's not null
        if (conn != null && !conn.isClosed()) {
            conn.close();
        }
    }

    public boolean validateUserCredentials(String username, String password)  {
        boolean isAuthenticated = false;
        String hashedPassword ;
        try {
            dbConnect(); // open the database connection
            // Your authentication code should go here
            // For now, this method just checks against hard-coded credentials
            isAuthenticated = "javaee".equals(username) && "esigelec2021".equals(password);
            String query = "SELECT email, password from users";
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                try (ResultSet rs = pstmt.executeQuery()) {
                    while (rs.next()) {
                    	hashedPassword = HashingUtil.hashPassword(password);
                        if(rs.getString("email").equals(username) && rs.getString("password").equals(hashedPassword)) {
                        	isAuthenticated = true;
                        }
                    }
                } catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        } catch (SQLException e) {
            // Handle SQL exceptions here
            e.printStackTrace();
        } finally {
            try {
                dbClose(); // close the database connection
            } catch (SQLException e) {
                // Handle potential exceptions when closing the connection
                e.printStackTrace();
            }
        }
        return isAuthenticated;
    }
    public String getCountryNameDb(String countryCode) {
        String countryName = null;
        try {
            dbConnect(); // open the database connection

            // Here, you would write the SQL query to fetch the country name from the database
            String query = "SELECT cname FROM country_capitals WHERE ccode = ?";
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setString(1, countryCode);

                try (ResultSet rs = pstmt.executeQuery()) {
                    if (rs.next()) {
                        countryName = rs.getString("cname");
                    }
                }
            }
        } catch (SQLException e) {
            // Handle exceptions
            e.printStackTrace();
        } finally {
            try {
                dbClose(); // close the database connection
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return countryName;
    }
    
    public String getCityNameDb(String countryCode) {
        String cityName = null;
        try {
            dbConnect(); // open the database connection

            // Here, you would write the SQL query to fetch the city name from the database
            String query = "SELECT ccapital FROM country_capitals WHERE ccode = ?";
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                pstmt.setString(1, countryCode);

                try (ResultSet rs = pstmt.executeQuery()) {
                    if (rs.next()) {
                        cityName = rs.getString("ccapital");
                    }
                }
            }
        } catch (SQLException e) {
            // Handle exceptions
            e.printStackTrace();
        } finally {
            try {
                dbClose(); // close the database connection
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cityName;
    }
    public List<String> getCountryCode() {
        List<String> countryCod = new ArrayList<>();
        try {
            dbConnect(); // open the database connection

            // Here, you would write the SQL query to fetch the city name from the database
            String query = "SELECT ccode FROM country_capitals";
            try (PreparedStatement pstmt = conn.prepareStatement(query)) {
                try (ResultSet rs = pstmt.executeQuery()) {
                    while (rs.next()) {
                       countryCod.add(rs.getString("ccode"));
                    }
                }
            }
        } catch (SQLException e) {
            // Handle exceptions
            e.printStackTrace();
        } finally {
            try {
                dbClose(); // close the database connection
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return countryCod;
    }
    public void addUser(String name, String email, String hashedPassword) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            // Open the database connection
            dbConnect();
            
            // SQL INSERT statement to add a new user
            String sql = "INSERT INTO users (name, email, password) VALUES (?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            
            // Set the values for the prepared statement
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.setString(3, hashedPassword);
            
            // Execute the insert operation
            pstmt.executeUpdate();
        } catch (SQLException e) {
            // Re-throw the exception to be handled in the servlet
            throw e;
        } finally {
            // Close PreparedStatement and database connection
            if (pstmt != null) {
                pstmt.close();
            }
            dbClose();
        }
    }
    public void addCountry(String code, String name, String capital) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            // Open the database connection
            dbConnect();
            
            // SQL INSERT statement to add a new user
            String sql = "INSERT INTO country_capitals (ccode, cname, ccapital) VALUES (?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            
            // Set the values for the prepared statement
            pstmt.setString(1, code);
            pstmt.setString(2, name);
            pstmt.setString(3, capital);
            
            // Execute the insert operation
            pstmt.executeUpdate();
        } catch (SQLException e) {
            // Re-throw the exception to be handled in the servlet
            throw e;
        } finally {
            // Close PreparedStatement and database connection
            if (pstmt != null) {
                pstmt.close();
            }
            dbClose();
        }
    }

}
