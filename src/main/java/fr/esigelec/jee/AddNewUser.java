package fr.esigelec.jee;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.esigelec.jee.dao.DBDAO;

/**
 * Servlet implementation class AddNewUser
 */
@WebServlet("/AddNewUser")
public class AddNewUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name = request.getParameter("name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        
        try {
            // Attempt to add the new user to the database
            // You'll need to implement the addUser method based on your database logic
        	String hashedPassword = HashingUtil.hashPassword(password);
            DBDAO dbDao = new DBDAO();
            dbDao.addUser(name, email, hashedPassword);
            
            // Redirect back to the login form with a success message
            response.sendRedirect("LoginForm.jsp?success=true");
        } catch (Exception e) {
            // Log exception (e.g., e.printStackTrace(); or logging framework)
            // Redirect back to the login form with an error message
            response.sendRedirect("LoginForm.jsp?error=true");
        }
	}
	

}
