package fr.esigelec.jee;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.esigelec.jee.dao.DBDAO;

/**
 * Servlet implementation class UserLogin
 */
@WebServlet("/UserLogin")
public class UserLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		    HttpSession session = request.getSession();
		    String username = request.getParameter("user");
	        String password = request.getParameter("pass");
	        // Check if any parameter is missing
	        if (username == null || password == null) {
	            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "BAD REQUEST");
	            return;
	        }

	        // Check if parameters are empty
	        if (username.trim().isEmpty() || password.trim().isEmpty()) {
	            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "BAD REQUEST");
	            return;
	        }

	        DBDAO dbdao = new DBDAO();
	        boolean isValidUser = dbdao.validateUserCredentials(username, password);

	        response.setContentType("text/html");
	        if (!isValidUser) {
	            // If user credentials are not correct, send a 403 status code
	        	String message = "<div class=\"alert alert-danger alert-dismissible\">\r\n"
	        			+ "							    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\r\n"
	        			+ "							    incorrecte\r\n"
	        			+ "							 </div>";
	        	request.setAttribute("msgerror", message);
	        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/LoginForm.jsp");
	            dispatcher.forward(request, response);
	            //response.sendRedirect("LoginForm.jsp");
	        } else {
	        	session.setAttribute("username", username);
	            // If user credentials are correct, redirect to the CountryCapital Servlet
	            response.sendRedirect("CountryCapitalName.jsp");
	           
	        }
	}

}
