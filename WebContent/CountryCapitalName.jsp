<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "fr.esigelec.jee.dao.DBDAO"%>
<%@ page import ="javax.servlet.http.Cookie" %>
<%@ page import="java.util.List" %>
<%String user = ""; 
if(session.getAttribute("username")!=null){
	user = (String)session.getAttribute("username");
}
else{
	response.sendRedirect("LoginForm.jsp");
}%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CountryCapital</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
</head>
<body>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#"><i class="fas fa-user-circle"></i><%=user %></a>
  <div class="collapse navbar-collapse">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="Logout">Logout</a>
      </li>
    </ul>
  </div>
</nav>
<%
Cookie[] cookies = request.getCookies();
DBDAO dbDAO = new DBDAO();
List<String> countryCodes = dbDAO.getCountryCode();
String country = "";
String capital = "";
boolean foundCookie = false;
String selectedCountryCode = request.getParameter("c");
%>

<% if (selectedCountryCode == null) {
    if (cookies != null) {
        for (Cookie cookie : cookies) {
            if ("CC".equals(cookie.getName())) {
                selectedCountryCode = cookie.getValue();
                foundCookie = true;
                break;
            }
        }
    }
    if (!foundCookie) {
        selectedCountryCode = countryCodes.get(0);
    }
} else {

    // Save the selected country code as a cookie for future visits
    Cookie countryCookie = new Cookie("CC", selectedCountryCode);
    countryCookie.setMaxAge(60 * 60 * 24); // Expires in one day
    response.addCookie(countryCookie);
}

// Fetch the country name and capital using the selected country code
country = dbDAO.getCountryNameDb(selectedCountryCode);
capital = dbDAO.getCityNameDb(selectedCountryCode);
%>
<div class="container text-center">
<h1 class="mt-5">World Countries Capital Web Application</h1>
<p class="lead mb-4">The capital of  <%=country %>  is  <%=capital %></p>
<button href='DeleteCountry?c=<%=selectedCountryCode %>' type="button" class="btn btn-danger">Delete <%= country %></button> &nbsp;&nbsp;&nbsp;
<button href='UpdateCountry.jsp?c=<%=selectedCountryCode %>' type="button" class="btn btn-warning">Update <%= country %></button>
<br>
<br>
<div><%
for (String code : countryCodes) {
    country = dbDAO.getCountryNameDb(code);
    %> <a href='CountryCapitalName.jsp?c=<%=code %>' class="btn btn-secondary"><%=country %>
            </a> &nbsp;&nbsp;
<%} %>
<jsp:include page="footer.jsp" flush="true" />
</div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.10/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>