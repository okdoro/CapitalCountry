<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Add New Country</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
<% String msg = (String)request.getAttribute("msg"); %>
   <div class="container mt-5">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title text-center bg-primary text-white text-center p-2">Add new User</h3>
    <form action="AddNewCountry" method="POST">
    <div class="form-group">
    <div><%= "true".equals(msg)? msg : "" %></div>
        <label for="code">Code:</label>
        <input type="text" id="code" name="code" required><br>
    </div> 
    <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" required><br>
    </div>   
    <div class="form-group">
        <label for="capital">Capital:</label>
        <input type="text" id="capital" name="capital" required><br>
    </div> 
        <input type="submit" value="Submit">
    </form>
    <a href="CountryCapitalName.jsp">Back to Home</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>