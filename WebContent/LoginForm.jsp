<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%String message = (String) request.getAttribute("msgerror");
    String success = request.getParameter("success");
    String error = request.getParameter("error");
    %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Form</title>
    <!-- Ajouter le lien CDN pour Bootstrap 4.5 CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title text-center bg-primary text-white text-center p-2">Please enter your login information</h3>
                        <%= "true".equals(success) ? "<p>User created successfully!</p>" : "true".equals(error) ? "<p>Error: Could not create user.</p>" : "" %>
                        <!-- Affichage du message (s'il existe) -->
                        <form action="UserLogin" method="POST">
                            <div class="form-group">
                                <label for="user">E-mail address:</label>
                                <input type="text" class="form-control" id="user" name="user" required>
                            </div>
                            <div class="form-group">
                                <label for="pass">Password:</label>
                                <input type="password" class="form-control" id="pass" name="pass" required>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Submit</button>
                             <%=message != null ? message : "" %>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="container mt-5"																																																																				>
    <div class="alert alert-primary alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    This site uses cookies. By continuing to browse this site, you are agreeing to our use of cookies.
  </div>
 </div>
    <!-- Ajouter le lien CDN pour Bootstrap 4.5 JS et les dépendances -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.5.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</body>
</html>
